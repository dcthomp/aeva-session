set(data
  oks003_ACL_AGS_LVTIT.med
  oks003_FMB_AGS_LVTIT.med
  oks003_FMC_AGS_03_LVTIT.med
)

# Install sample data for use by the aeva application
smtk_get_kit_name(name dir_prefix)
install(
  FILES
    ${data}
  DESTINATION
    share/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix}
)
