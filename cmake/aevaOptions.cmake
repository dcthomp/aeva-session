include(CMakeDependentOption)
option(AEVA_ENABLE_TESTING "Enable tests." ON)
option(AEVA_ENABLE_PYTHON "Build python bindings for aeva." ON)
# Provide system packagers with the ability to install SMTK
# to the system's Python site package directory. The default
# is off so that people building relocatable bundles (such as
# CMB) can distribute their own Python interpreter with
# internal packages (e.g., as part of a Mac ".app" bundle).
cmake_dependent_option(
  AEVA_INSTALL_PYTHON_TO_SITE_PACKAGES
  "Install Python modules to the interpreter's site-packages directory or into CMAKE_INSTALL_PREFIX?"
  OFF
  AEVA_ENABLE_PYTHON OFF)
mark_as_advanced(AEVA_INSTALL_PYTHON_TO_SITE_PACKAGES)
