<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA "ExportFeb" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="export feb" Label="export model to FEBio" BaseType="operation">
      <AssociationsDef Name="models" Label="model(s)" NumberOfRequiredValues="1" Extensible="true">
        <!-- TODO should include new property filter string for "aeva_datatype" -->
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="model"/></Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="filename" NumberOfRequiredValues="1" Extensible="true"
          ShouldExist="false"
          FileFilters="FEBio Data (*.feb)">
        </File>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(exportfeb)" BaseType="result">
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="export model to FEBio" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="export feb">
          <ItemViews>
            <View Path="/models" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="Components"
                    Filter="model"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
