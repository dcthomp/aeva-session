//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_AdjacencyFeature_h
#define smtk_session_aeva_AdjacencyFeature_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

#include "vtkNew.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class SMTKAEVASESSION_EXPORT AdjacencyFeature : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::AdjacencyFeature);
  smtkCreateMacro(AdjacencyFeature);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

  bool ableToOperate() override;

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_AdjacencyFeature_h
