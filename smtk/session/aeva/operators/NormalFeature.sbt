<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "normal feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="normal feature" Label="select by normal" BaseType="operation">

      <BriefDescription>Generate a surface selection by choosing cells with normals in a given direction.</BriefDescription>
      <DetailedDescription>
        Generate a surface selection by choosing cells with normals in a direction.

        Note that the direction vector must not be zero-length.
        It is acceptable if it is not unit-length (it will be normalized as needed).
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1" Extensible="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="direction" Label="normal direction" NumberOfRequiredValues="3">
          <BriefDescription>Cells with normals in this direction will be added to the feature.</BriefDescription>
          <DefaultValue>0.0,0.0,1.0</DefaultValue>
        </Double>

        <Double Name="angle" Label="angle threshold" Units="degrees" NumberOfRequiredValues="1">
          <BriefDescription>The maximum deviation from the normal direction that will be accepted.</BriefDescription>
          <DefaultValue>45.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Max Inclusive="true">360.</Max>
          </RangeInfo>
        </Double>

        <String Name="save result as" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>When enabled, the resulting primitives are saved
            as persistent reference geometry. When disabled, as an ephemeral
            selection rather that can be further edited by user interaction.
          </BriefDescription>
          <DetailedDescription>
            By default in the graphical interface, this operation will create
            an ephemeral selection that can be modified with interactive tools
            before being "frozen" into persistent reference geometry.
            Scripts may enable this item to force persistent reference
            geometry to be created immediately rather than requiring a
            "Duplicate" operation to do so.
          </DetailedDescription>
        </String>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(normal feature)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="select by normal" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="normal feature">
          <ItemViews>
            <View Path="/source" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="face"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
