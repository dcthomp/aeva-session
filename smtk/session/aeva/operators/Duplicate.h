//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_Duplicate_h
#define smtk_session_aeva_Duplicate_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief Duplicate cell(s) as side set(s).
  *
  * This creates a copy of any cells associated to the operation.
  * The new cells will be side sets (i.e., references to primary
  * geometry), not primary geometry.
  */
class SMTKAEVASESSION_EXPORT Duplicate : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::Duplicate);
  smtkCreateMacro(Duplicate);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_Duplicate_h
