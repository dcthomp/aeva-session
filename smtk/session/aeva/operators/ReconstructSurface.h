//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_ReconstructSurface_h
#define smtk_session_aeva_ReconstructSurface_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief Reconstruct a surface using its points and point normals.
  *
  * Given a surface with point normals, the dimensions of an
  * enclosing voxel grid and a radius of influence, apply a
  * variant of Curless et al.:
  *
  * Brian Curless and Marc Levoy. 1996. A volumetric method for
  * building complex models from range images. In Proceedings of
  * the 23rd annual conference on Computer graphics and
  * interactive techniques (SIGGRAPH ’96). Association for
  * Computing Machinery, New York, NY, USA, 303–312.
  * DOI:https://doi.org/10.1145/237170.237269
  *
  * This method constructs an implicit signed distance function
  * from the input dataset's points and point normals, using the
  * radius of influence to define a local neighborhood for
  * computing the signed distance. It then computes an isocontour
  * at distance = 0 using a variant of the Flying Edges technique:
  *
  * W. Schroeder, R. Maynard and B. Geveci, "Flying edges: A
  * high-performance scalable isocontouring algorithm," 2015 IEEE 5th
  * Symposium on Large Data Analysis and Visualization (LDAV), Chicago,
  * IL, 2015, pp. 33-40, doi: 10.1109/LDAV.2015.7348069.
  *
  * \sa vtkSignedDistance, vtkExtractSurface
  */
class SMTKAEVASESSION_EXPORT ReconstructSurface : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::ReconstructSurface);
  smtkCreateMacro(ReconstructSurface);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_ReconstructSurface_h
