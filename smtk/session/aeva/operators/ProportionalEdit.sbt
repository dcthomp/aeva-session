<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proportional edit" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="proportional edit" Label="proportional edit" BaseType="operation">
      <BriefDescription>Deform a surface using vtk proportional editing filter</BriefDescription>
      <DetailedDescription>
        Deform a surface using vtk proportional editing filter.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input surface to deform</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Group Name="influence region" Label="influence region" NumberOfGroups="1">
          <ItemDefinitions>
            <Double Name="influence radius" Label="radius of influence">
              <BriefDescription>Radius of influence</BriefDescription>
              <DefaultValue>5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>

            <Int Name="projection enabled" Label="projection enabled" NumberOfRequiredValues="1">
              <BriefDescription>Specify whether projection mode is enabled.</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Int>

            <Double Name="anchor point" Label="center point" NumberOfRequiredValues="3">
              <BriefDescription>Center point coordinate of the influence region</BriefDescription>
              <DefaultValue>0., 0., 0.</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>

            <Double Name="displacement vector" Label="displacement" NumberOfRequiredValues="3">
              <BriefDescription> Destination vector for the displacement</BriefDescription>
              <DefaultValue>20., 0., 0.</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>

            <Double Name="projection vector" Label="direction of projection" NumberOfRequiredValues="3">
              <BriefDescription>Direction of projection in 3D</BriefDescription>
              <DefaultValue>0., 0., 1.</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>

          </ItemDefinitions>
        </Group>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(proportional edit)" BaseType="result"/>
  </Definitions>
  <Views>
    <View
      Type="Operation"
      Title="proportional edit"
      TopLevel="true"
      FilterByAdvanceLevel="false"
      FilterByCategoryMode="false"
    >
      <InstancedAttributes>
        <Att Type="proportional edit">
          <ItemViews>
            <View Item="influence region"
              Type="ProportionalEditSphere"
              InfluenceRadius="influence radius"
              ProjectionEnabled="projection enabled"
              AnchorPoint="anchor point"
              Displacement="displacement vector"
              Projection="projection vector"
              ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
