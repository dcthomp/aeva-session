<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proximity feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="boolean intersect" Label="boolean intersect" BaseType="operation">

      <BriefDescription>Perform a set-based boolean intersection.</BriefDescription>
      <DetailedDescription>
        The output workpiece will contain every primitive
        that appears in either the input workpiece or the tools, but not in both.
      </DetailedDescription>
      <AssociationsDef Name="workpieces" NumberOfRequiredValues="2">
        <BriefDescription>Side sets to be intersected.</BriefDescription>
          <!-- TODO: Accepts should include only side sets -->
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="any"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Void Name="keep inputs" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>Should the workpiece and tool objects survive the operation?</BriefDescription>
          <DetailedDescription>
            Should the workpiece and tool objects survive the operation?
            If so, a new side set is created for the result.
            If not, then the first workpiece assigned will have its geometry replaced with
            the resulting intersection and the second will be deleted.
          </DetailedDescription>
        </Void>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(boolean intersect)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" Optional="true" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="boolean intersect" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="boolean intersect">
          <ItemViews>
            <View Path="/workpieces" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="ComponentsWithGeometry"
                    Filter="any"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
