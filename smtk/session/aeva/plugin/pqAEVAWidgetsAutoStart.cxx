//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/plugin/pqAEVAWidgetsAutoStart.h"

#include "smtk/view/Selection.h"

#include "smtk/session/aeva/CompilerInfo.h"
#include "smtk/session/aeva/plugin/pqSMTKProportionalEditItemWidget.h"
#include "smtk/session/aeva/plugin/pqSMTKVolumeInspectionItemWidget.h"

#include "smtk/extension/paraview/widgets/pqSMTKAttributeItemWidget.h"
#include "smtk/extension/qt/qtSMTKUtilities.h"
#ifdef SMTK_ENABLE_MARKUP
AEVA_THIRDPARTY_PRE_INCLUDE
#include "smtk/markup/ontology/OwlRdfSource.h"
AEVA_THIRDPARTY_POST_INCLUDE
#endif // SMTK_ENABLE_MARKUP

#include "pqApplicationCore.h"
#include "pqObjectBuilder.h"

#include "uberon_cpp.h"

pqAEVAWidgetsAutoStart::pqAEVAWidgetsAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqAEVAWidgetsAutoStart::~pqAEVAWidgetsAutoStart() = default;

// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
void pqAEVAWidgetsAutoStart::startup()
{
  /*
  auto pqCore = pqApplicationCore::instance();
  if (pqCore)
  {
  }
  */

  // Register qtItem widget subclasses implemented using ParaView 3-D widgets:
  qtSMTKUtilities::registerItemConstructor(
    "ProportionalEditSphere", pqSMTKProportionalEditItemWidget::createSphereItemWidget);
  qtSMTKUtilities::registerItemConstructor(
    "ProportionalEditCylinder", pqSMTKProportionalEditItemWidget::createCylinderItemWidget);
  qtSMTKUtilities::registerItemConstructor(
    "VolumeInspectionPlanes", pqSMTKVolumeInspectionItemWidget::createVolumeInspectionItemWidget);

  // Tell SMTK that we want 3D widgets to persist, not disappear when switching tabs
  pqSMTKAttributeItemWidget::setHideWidgetWhenInactive(false);

  // Register the Uberon ontology with qtOntologyModel.
  smtk::markup::ontology::OwlRdfSource uberon{
    uberon_cpp(), "http://purl.obolibrary.org/obo/uberon.owl", "uberon"
  };
  smtk::markup::ontology::Source::registerSource(uberon);
}

void pqAEVAWidgetsAutoStart::shutdown() {}
