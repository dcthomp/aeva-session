//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/plugin/ApplicationConfiguration.h"

#include "smtk/session/aeva/OperationDecorator.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKDiagramPanel.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKOperationToolboxPanel.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResourcePanel.h"
#include "smtk/view/Configuration.h"
#include "smtk/view/Information.h"
#include "smtk/view/json/jsonView.h"

#include "smtk/session/aeva/plugin/DiagramConfiguration_cpp.h"

ApplicationConfiguration::ApplicationConfiguration(QObject* parent)
  : QObject(parent)
{
}

smtk::view::Information ApplicationConfiguration::panelConfiguration(const QWidget* panel)
{
  smtk::view::Information result;
  if (const auto* toolbox = dynamic_cast<const pqSMTKOperationToolboxPanel*>(panel))
  {
    nlohmann::json jsonConfig = { { "Name", "Operations" },
      { "Type", "qtOperationPalette" },
      { "Component",
        { { "Name", "Details" },
          { "Attributes",
            { { "SearchBar", true }, { "Title", "Tools" }, { "SubsetSort", "Precedence" } } },
          { "Children",
            { { { "Name", "Model" }, { "Attributes", { { "Autorun", "true" } } } } } } } } };
    std::shared_ptr<smtk::view::Configuration> viewConfig = jsonConfig;
    result.insertOrAssign(viewConfig);

    auto operations = std::make_shared<smtk::session::aeva::OperationDecorator>();
    // Ensure we insert the decorator as a base-type shared-pointer
    // so it can be retrieved properly:
    result.insertOrAssign(std::dynamic_pointer_cast<smtk::view::OperationDecorator>(operations));
  }
  else if (const auto* browser = dynamic_cast<const pqSMTKResourcePanel*>(panel))
  {
    (void)browser;
    // Fetch the default JSON configuration for the resource-browser panel.
    auto jsonConfig = nlohmann::json::parse(pqSMTKResourceBrowser::getJSONConfiguration())[0];
#ifdef SMTK_ENABLE_MARKUP
    // Override the default subphrase generator:
    jsonConfig["Component"]["Children"][0]["Children"][0]["Attributes"]["Type"] =
      "smtk::markup::SubphraseGenerator";
#endif
    std::shared_ptr<smtk::view::Configuration> viewConfig = jsonConfig;
    result.insertOrAssign(viewConfig);
  }
  else if (const auto* diagram = dynamic_cast<const pqSMTKDiagramPanel*>(panel))
  {
    (void)diagram;
    // Fetch the default JSON configuration for the resource-browser panel.
    nlohmann::json jsonConfig = nlohmann::json::parse(diagramPanelConfiguration())[0];
    std::shared_ptr<smtk::view::Configuration> viewConfig = jsonConfig;
    result.insertOrAssign(viewConfig);
  }
  else
  {
    smtkWarningMacro(smtk::io::Logger::instance(),
      "Unknown panel named \"" << (panel ? panel->objectName().toStdString() : "null") << "\"\n");
  }
  return result;
}
