set(classes
  vtkAppendFilterIntIds
  vtkDistanceToFeature
  vtkGlobalIdBooleans
  vtkGrowCharts
  vtkImageNarrowBand
  vtkLSCMFilter
  vtkMedHelper
  vtkMedReader
  vtkMedWriter
  vtkNewFeatureEdges
  vtkProportionalEditElements
  vtkProportionalEditFilter
  vtkProportionalEditRepresentation
  vtkProportionalEditWidget
  vtkSideSetsToScalars
  vtkStarIterator
  vtkTexturePackingFilter
)

set(headers
  vtkVisitation.h
)

if(SMTK_ENABLE_PARAVIEW_SUPPORT)
  list(APPEND classes
    vtkPVImageSliceMapper2
    vtkVolumeInspectionRepresentation
    vtkVolumeInspectionWidget
  )
endif()

vtk_module_add_module(VTK::AEVAExt
  CLASSES ${classes}
  HEADERS ${headers}
  HEADERS_SUBDIR "vtk/aeva/ext")

target_include_directories(AEVAExt
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}>
)

if (AEVA_ENABLE_TESTING)
  add_subdirectory(Testing)
endif()
